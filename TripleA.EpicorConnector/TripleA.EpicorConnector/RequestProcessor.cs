﻿/*
 * Copyright (c) 2012 Epicor Software..  All Rights Reserved.
 *
 * EPICOR SOFTWARE.  MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 * EPICOR SOFTWARE SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * $Workfile:   RequestProcessor.cs  $
 * $Id$
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Xml;
using System.Diagnostics;
using com.epicor.acxclsdk.acxclapi;
using TripleA.DataAccess;
using TripleA.DataAccess.Repositories;
using System.Linq;
using TripleA.DataAccess.Repositories.Dto;
using System.Threading.Tasks;
using TripleA.SearchDecorator.SearchDecorator;

namespace TripleA.EpicorConnector
{

    /**
     *
     * @author Sergio Peralta
     */
    public class RequestProcessor
    {
        protected log4net.ILog logger = null;

        public String progname = null;
        public int delay = 0;
        // some debug testing values.
        static readonly String BAD_CUST_NUM = "BadCustNum";
        static readonly String ERR_REQ_GEN = "ErrorRequest_GENERAL";
        static readonly String ERR_REQ_UNK = "ErrorRequest_UNKNOWN";
        static readonly String ERR_REQ_TOUT = "ErrorRequest_TIMEOUT";

        public RequestProcessor(int delay, String progname)
        {
            this.progname = progname;
            this.delay = delay;
            logger = log4net.LogManager.GetLogger(((Object)this).GetType().FullName);
        }
        // This is a simple inquiry/order "processor" that just copies info 
        // from the request into the response and supplies hard coded quantity and
        // price info.
        public String processRequest(MsgInfo msginfo)
        {

            int transtype = checkRequest(msginfo.request, msginfo.timeout);
            String env = null;
            String router = null;
            String buyid = null;
            String sellid = null;

            switch (transtype)
            {
                case AppSDKConst.TRANS_TYPE_INVOICE:
                case AppSDKConst.TRANS_TYPE_PART_INQUIRY:
                case AppSDKConst.TRANS_TYPE_CART_INQUIRY:
                case AppSDKConst.TRANS_TYPE_TIRE_INQUIRY:
                case AppSDKConst.TRANS_TYPE_PART_ORDER:
                case AppSDKConst.TRANS_TYPE_TIRE_ORDER:
                case AppSDKConst.TRANS_TYPE_STOCK_ORDER:
                case AppSDKConst.TRANS_TYPE_PART_RETURN:
                    // all of the above get the router from
                    // <RequestRouter>
                    router = AppSDKXMLUtil.getTagValue("RequestRouter", msginfo.request);
                    break;
                case AppSDKConst.TRANS_TYPE_PRODUCT_COVERAGE:
                    router = AppSDKXMLUtil.getTagValue("ProductCoverageRequest", msginfo.request);
                    break;
                case AppSDKConst.TRANS_TYPE_PRODUCT_LINE_MAP:
                    router = AppSDKXMLUtil.getTagValue("ProductLineMapRequest", msginfo.request);
                    break;
                case AppSDKConst.TRANS_TYPE_CATALOG_PROFILE:
                    router = AppSDKXMLUtil.getTagValue("CatalogRequest", msginfo.request);
                    break;
                default:
                    router = msginfo.request;
                    break;
            }

            //TODO: buscar el sellerId y el partnerId
            env = AppSDKXMLUtil.getTagValue("Envelope", msginfo.request);

            // Now pull the BuyPartnerID and SellPartnerID out of the env and router xml.
            buyid = AppSDKXMLUtil.getTagValue("BuyPartnerID", env);
            sellid = AppSDKXMLUtil.getTagValue("SellPartnerID", router);

            if (delay > 0)
            {
                try
                {
                    // sleep delay seconds to simulate a delay
                    Thread.Sleep(delay * 1000);
                }
                catch (ThreadInterruptedException) { }
            }

            switch (transtype)
            {
                case AppSDKConst.TRANS_TYPE_INVOICE:
                    return processInvoice(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_PART_INQUIRY:
                    return processInquiry(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_CART_INQUIRY:
                    return processCartInquiry(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_TIRE_INQUIRY:
                    return processTireInquiry(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_PART_ORDER:
                    return processOrder(msginfo.request, msginfo.ACXTrackNum, false, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_TIRE_ORDER:
                    return processTireOrder(msginfo.request, msginfo.ACXTrackNum, false, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_STOCK_ORDER:
                    return processOrder(msginfo.request, msginfo.ACXTrackNum, true, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_PART_RETURN:
                    return processReturn(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_CATALOG_PROFILE:
                    return processCatalogProfile(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_PRODUCT_COVERAGE:
                    return processProductCoverage(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                case AppSDKConst.TRANS_TYPE_PRODUCT_LINE_MAP:
                    return processProductLineMap(msginfo.request, msginfo.ACXTrackNum, msginfo.timeout);
                default:
                    throw new AppSDKException("Unrecognized transaction type " + transtype);
            }
        }

        // this method is used to figure out what type of document we have and to
        // implement some simulated error conditions.
        int checkRequest(String xml, int timeout)
        {
            int transtype = 0;

            XmlDocument document = new XmlDocument();
            StringReader xmlStream = new StringReader(xml);

            document.XmlResolver = null;

            document.Load(xmlStream);
            XmlNode root = document.DocumentElement;
            String xmlDoctype = root.Name;


            if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_PART_INQUIRY))
                transtype = AppSDKConst.TRANS_TYPE_PART_INQUIRY;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_CART_INQUIRY))
                transtype = AppSDKConst.TRANS_TYPE_CART_INQUIRY;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_TIRE_INQUIRY))
                transtype = AppSDKConst.TRANS_TYPE_TIRE_INQUIRY;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_INVOICE))
                transtype = AppSDKConst.TRANS_TYPE_INVOICE;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_PART_ORDER))
                transtype = AppSDKConst.TRANS_TYPE_PART_ORDER;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_TIRE_ORDER))
                transtype = AppSDKConst.TRANS_TYPE_TIRE_ORDER;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_STOCK_ORDER))
                transtype = AppSDKConst.TRANS_TYPE_STOCK_ORDER;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_CATALOG_PROFILE))
                transtype = AppSDKConst.TRANS_TYPE_CATALOG_PROFILE;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_PRODUCT_COVERAGE))
                transtype = AppSDKConst.TRANS_TYPE_PRODUCT_COVERAGE;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_PRODUCT_LINE_MAP))
                transtype = AppSDKConst.TRANS_TYPE_PRODUCT_LINE_MAP;
            else if (xmlDoctype.Equals(AppSDKConst.TRANS_TYPE_DOC_PART_RETURN))
                transtype = AppSDKConst.TRANS_TYPE_PART_RETURN;
            else
            {
                throw new ServiceProviderException(ErrorConst.SYS_ERR_NOT_SUPPORTED, "Unknown document type " + xmlDoctype);
            }

            String custNum = AppSDKXMLUtil.getTagValue("CustNum", xml);
            String docMsg = AppSDKXMLUtil.getTagValue("DocGenBy", xml);

            if (BAD_CUST_NUM.Equals(custNum, StringComparison.OrdinalIgnoreCase))
            {
                throw new ServiceProviderException(ErrorConst.PORDER_APP_ERR_INVALID_CUSTOMER,
                              "Customer account " + custNum + " is invalid");
            }
            if (ERR_REQ_GEN.Equals(docMsg, StringComparison.OrdinalIgnoreCase))
            {
                throw new ServiceProviderException(ErrorConst.SYS_ERR_GENERAL, "Generating General error as requested");
            }
            else if (ERR_REQ_TOUT.Equals(docMsg, StringComparison.OrdinalIgnoreCase))
            {
                try { Thread.Sleep(timeout * 1000); }
                catch (ThreadInterruptedException) { }

            }
            else if (ERR_REQ_UNK.Equals(docMsg, StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("Generating Unexpected exception as requested");
            }

            return transtype;
        }
        public String createNotificationResponse(String xml, String trackNum, int code, String msg)
        {
            StringBuilder buf = new StringBuilder(1024);
            buf.Append("<ACXNotificationResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            buf.Append("<NotificationResponse><ACXTrackNum>");
            buf.Append(trackNum);
            buf.Append("</ACXTrackNum><SellPartnerID>");
            buf.Append(AppSDKXMLUtil.getTagValue("SellPartnerID", xml));
            buf.Append("</SellPartnerID><Severity>ERROR</Severity><Code>");
            buf.Append(code);
            buf.Append("</Code><Msg>");
            buf.Append(AppSDKXMLUtil.quoteContent(msg)); //be sure to escape any special
                                                         //characters in the error message
            buf.Append("</Msg></NotificationResponse>");

            buf.Append("</ACXNotificationResponse>");
            return buf.ToString();
        }

        String processInvoice(String xml, String trackNum, int timeout)
        {
            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_INVOICE_RESPONSE));
            buf.Append("<ACXInvoiceResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            appendResponseRouter(buf,
                                 AppSDKXMLUtil.getTagValue("SellPartnerID", xml),
                                 trackNum);

            appendInvoiceResponseItems(buf, xml);

            buf.Append("</ACXInvoiceResponse>");
            return buf.ToString();
        }

        String processInquiry(String xml, String trackNum, int timeout)
        {
            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_PART_INQUIRY_RESPONSE));
            buf.Append("<ACXPartInquiryResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            appendResponseRouter(buf,
                                 AppSDKXMLUtil.getTagValue("SellPartnerID", xml),
                                 trackNum);

            List<ProductDto> products = new List<ProductDto>();
            buf.Append("<PartInquiryResponse>");
            appendResponseItems(buf, xml, ref products);
            buf.Append("</PartInquiryResponse>");

            buf.Append("</ACXPartInquiryResponse>");
            Console.WriteLine(buf.ToString());
            return buf.ToString();
        }

        String processCartInquiry(String xml, String trackNum, int timeout)
        {
            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_CART_INQUIRY_RESPONSE));
            buf.Append("<ACXCartInquiryResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            appendResponseRouter(buf,
                                 AppSDKXMLUtil.getTagValue("SellPartnerID", xml),
                                 trackNum);

            buf.Append("<CartInquiryResponse>");
            appendCartResponseItems(buf, xml);
            buf.Append("</CartInquiryResponse>");

            buf.Append("</ACXCartInquiryResponse>");
            return buf.ToString();
        }

        String processTireInquiry(String xml, String trackNum, int timeout)
        {
            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_TIRE_INQUIRY_RESPONSE));
            buf.Append("<ACXTireInquiryResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            appendResponseRouter(buf,
                                 AppSDKXMLUtil.getTagValue("SellPartnerID", xml),
                                 trackNum);

            buf.Append("<TireInquiryResponse>");
            appendTireInquiryResponseItems(buf, xml);
            buf.Append("</TireInquiryResponse>");

            buf.Append("</ACXTireInquiryResponse>");
            return buf.ToString();
        }

        OrderDto OrderMapping(int userId, string orderId, cat_clientes customer, cat_tiendas store, List<ProductDto> products, string comment)
        {
            var date = DateTime.Now;
            var dateFormatted = new DateTime(date.Year, date.Month, date.Day);
            var order = new OrderDto()
            {
                UserId = userId, //read this value from appsettings,
                OrderIdMp4 = orderId,
                IdCatVendedor = userId,
                OrderDate = dateFormatted,
                OrderEndDate = dateFormatted.AddDays(7),
                CustomerId = customer.id_cat_clientes,
                CustomerName = customer?.nombre,
                IdCatTiendas = store.id_cat_tiendas,
                ISV = products.Sum(a => a.ISV().GetValueOrDefault(0)),
                Subtotal = products.Sum(a => a.SubTotal()).GetValueOrDefault(0),
                Total = products.Sum(a => a.Total()).GetValueOrDefault(0),
                Discount = 0,
                Comments = comment,
                Lines = products.Select(a =>
                    new OrderDetail
                    {
                        Id = a.Id,
                        Cost = a.Cost,
                        ISV = a.ISV(),
                        Subtotal = a.SubTotal(),
                        Price = a.Price,
                        Qty = a.RequestQty,
                        Total = a.Total(),
                    }
            ).ToList()
            };

            return order;
        }


        String processOrder(String xml, String trackNum, Boolean stock, int timeout)
        {
            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml,
                 stock ? AppSDKConst.TRANS_TYPE_STOCK_ORDER_RESPONSE : AppSDKConst.TRANS_TYPE_PART_ORDER_RESPONSE));
            buf.Append(stock ? "<ACXStockOrderResponse>" : "<ACXPartOrderResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            appendResponseRouter(buf,
                                 AppSDKXMLUtil.getTagValue("SellPartnerID", xml),
                                 trackNum);

            buf.Append("<PartOrderResponse>");

            // ignore PartRequestAction, ItemDefaults
            List<ProductDto> products = new List<ProductDto>();
            appendResponseItems(buf, xml, ref products);
            var comment = AppSDKXMLUtil.getTagValue("OrderMsg", xml);
            var customerNum = AppSDKXMLUtil.getTagValue("CustNum", xml);          

            try
            {
                int clientId = int.Parse(customerNum);
                var username = ConfigurationManager.AppSettings["defaultUser"];

                var context = new retailsystem_n3a_t1_testEntities();
                var customerRepository = new ClientRepository(context);
                var userRepository = new UserRepository(context);
                var customer = customerRepository.Find(clientId);
                var user = userRepository.FindByCode(username);

                if (customer is null)
                {
                    var errmsg = $"Failed to find the client: {clientId}";
                    Console.WriteLine(errmsg);
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(xml, trackNum, 4102, errmsg));
                    debugln(buf.ToString());
                    return buf.ToString();
                }

                var orderId = "ORDER-" + trackNum;
                var storeRepository = new StoreRepository(context);
                var store = storeRepository.FindByCode(customer?.codigo_tienda);             
                
                //getting for the customer assigned store.
                context.Database.Connection.ConnectionString = store?.cnn;
                store = storeRepository.FindByCode(customer?.codigo_tienda);
                var newOrder = OrderMapping(user.id_cat_usuarios, orderId, customer, store, products, comment);

                OrderRepository orderRepository = new OrderRepository(context);
                orderRepository.Create(newOrder);

                //send the notification mail
                var message = new MailServiceDto
                {
                    AddTo = store.mail ?? "soporte@tripleahn.com",
                    From = "soporte@tripleahn.com",
                    PlainTextContent = $"Orden {orderId} ha sido creada, por el cliente: {customer.codigo} - {customer.nombre} ",
                    Subject = $"Nueva Orden MP4P en tienda {store.codigo} - {store.nombre}"
                };
                var task = Task.Run(() => MailService.SendMessageAsync(message));
                task.Wait();

                buf.Append("<ResponseOrder><ConfNum>").Append(orderId).Append("</ConfNum></ResponseOrder>");
                buf.Append("</PartOrderResponse>");

            } catch(Exception e)
            {
                Console.WriteLine(e.Message + " " + e.StackTrace);
                var errmsg = "Failed to create order";
                buf.Remove(0, buf.Length);
                buf.Append(createNotificationResponse(xml, trackNum, 4102, errmsg));
                debugln(buf.ToString());

                //send the notification mail
                var message = new MailServiceDto
                {
                    AddTo = "soporte@tripleahn.com",
                    From = "soporte@tripleahn.com",
                    PlainTextContent = e.Message,
                    Subject = $"Ha ocurrido un error"
                };
                var task = Task.Run(() => MailService.SendMessageAsync(message));
                task.Wait();
                return buf.ToString();
            }

            buf.Append(stock ? "</ACXStockOrderResponse>" : "</ACXPartOrderResponse>");
            return buf.ToString();
        }

        String processTireOrder(String xml, String trackNum, bool stock, int timeout)
        {
            String estDelivery = Utils.millisPastEpochToDateTimeGMT(Utils.CurrentTimeMillis()).ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
            String sellpartnerid = AppSDKXMLUtil.getTagValue("SellPartnerID", xml);

            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_TIRE_ORDER_RESPONSE));
            buf.Append("<ACXTireOrderResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            appendResponseRouter(buf, sellpartnerid, trackNum);
            // ignore PartRequestAction, ItemDefaults

            buf.Append("<TireOrderResponse>");
            appendTireOrderResponseItems(buf, xml);
            buf.Append("<ResponseOrder><Msg>in progress</Msg><ConfNum>").Append(
                       "ORDER-" + trackNum).Append("</ConfNum>");
            buf.Append("<EstDelivery>").Append(estDelivery).Append("</EstDelivery>");
            buf.Append("<PickUpPhone>5125551234</PickUpPhone>");
            buf.Append("<Location>");
            buf.Append("<Name>Tire company</Name>");
            buf.Append("<PartnerID>").Append(sellpartnerid).Append("</PartnerID>");
            buf.Append("<Address>");
            buf.Append("<Street>804 Las Cimas Pkwy</Street>");
            buf.Append("<City>Austin</City>");
            buf.Append("<State>TX</State>");
            buf.Append("<Country>US</Country>");
            buf.Append("<PostalID>78746</PostalID>");
            buf.Append("</Address>");
            buf.Append("</Location>");
            buf.Append("</ResponseOrder>");
            buf.Append("</TireOrderResponse>");

            buf.Append("</ACXTireOrderResponse>");
            return buf.ToString();
        }

        String processReturn(String xml, String trackNum, int timeout)
        {
            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_PART_RETURN_RESPONSE));
            buf.Append("<ACXPartReturnResponse>");
            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            appendResponseRouter(buf, AppSDKXMLUtil.getTagValue("SellPartnerID", xml), trackNum);
            // ignore PartRequestAction, ItemDefaults

            buf.Append("<PartReturnResponse>");
            buf.Append("<WelcomeMsg>Welcome to Joes Auto Parts</WelcomeMsg>");
            appendReturnItems(buf, xml);
            buf.Append("</PartReturnResponse>");

            buf.Append("</ACXPartReturnResponse>");
            return buf.ToString();
        }

        void appendEnvelope(StringBuilder buf, String buyPartnerID)
        {
            buf.Append("<Envelope><BuyPartnerID>");
            buf.Append(AppSDKXMLUtil.trim(buyPartnerID));
            buf.Append("</BuyPartnerID><DocVersNum>1.0</DocVersNum><DocGenBy>" + progname + "</DocGenBy></Envelope>");
        }

        void appendResponseRouter(StringBuilder buf, String sellPartnerID, String acxTrackNum)
        {
            buf.Append("<ResponseRouter><SellPartnerID>");
            buf.Append(AppSDKXMLUtil.trim(sellPartnerID));
            buf.Append("</SellPartnerID><ACXTrackNum>");
            buf.Append(AppSDKXMLUtil.trim(acxTrackNum));
            buf.Append("</ACXTrackNum></ResponseRouter>");
        }
        void appendReturnItems(StringBuilder buf, String xml)
        {
            int n = 0;
            String val;
            while ((val = AppSDKXMLUtil.getNthTagValue("RequestItem", xml, n++)) != null)
            {
                String id = AppSDKXMLUtil.getTagValue("ID", val);
                String pnum = AppSDKXMLUtil.getTagValue("PartNum", val);
                String lineCode = AppSDKXMLUtil.getTagValue("MfgCode", val);
                String reqQty = AppSDKXMLUtil.getTagValue("Qty", val);
                String price = AppSDKXMLUtil.getTagValue("UnitCost", val);
                // ignore all other RequestItem fields

                logger.Debug("Looking for:");
                logger.Debug("PartNumber:" + pnum);
                logger.Debug("LineCode: " + lineCode);

                buf.Append("<ResponseItem>");
                buf.Append("<Status value='1'/><ID>").Append(id).Append("</ID>");
                buf.Append("<PartNum>").Append(pnum).Append("</PartNum>");
                buf.Append("<MfgCode>").Append(lineCode).Append("</MfgCode>");
                buf.Append("<Qty>").Append(reqQty).Append("</Qty>");
                buf.Append("<CreditAmount>").Append(price).Append("</CreditAmount>");
                buf.Append("<Msg>Parts accepted.</Msg></ResponseItem>");
            }
        }

        private SearchInputDto searchInputParser(string storeCode, string val) 
            => new SearchInputDto()
        {
            Id = AppSDKXMLUtil.getTagValue("ID", val),
            BuyerDesc = AppSDKXMLUtil.getTagValue("BuyerDesc", val),
            LineCode = AppSDKXMLUtil.getTagValue("MfgCode", val),
            RequestQty = AppSDKXMLUtil.getTagValue("Qty", val),
            PartNumber = AppSDKXMLUtil.getTagValue("PartNum", val),
            StoreCode = storeCode
        };


        void appendResponseItems(StringBuilder buf, String xml, ref List<ProductDto> products)
        {          
            int r = 0, n = 0, id = -1;
            String val;
            String storeCode = string.Empty;
            string trackNum = Guid.NewGuid().ToString();
            var context = new retailsystem_n3a_t1_testEntities();

            while ((val = AppSDKXMLUtil.getNthTagValue("RequestRouter", xml, r++)) != null)
            {
                try
                {
                    var customerNum = AppSDKXMLUtil.getTagValue("CustNum", val);
                    id = int.Parse(customerNum);

                    var clientRepository = new ClientRepository(context);
                    var customer = clientRepository.Find(id);
                    storeCode = customer?.codigo_tienda;

                } catch(Exception e)
                {
                    Console.WriteLine(e.Message + " " + e.StackTrace);
                    var errmsg = $"Failed to Lookup for customer {id}";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(xml, trackNum, 4102, errmsg));
                    debugln(buf.ToString());
                }
            }

            logger.Debug("Looking in the database:");

            while ((val = AppSDKXMLUtil.getNthTagValue("RequestItem", xml, n++)) != null)
            {  
                try
                {
                    var input = searchInputParser(storeCode, val);
                    logger.Debug("PartNo:" + input.LineCode + "+" + input.PartNumber + " - Store:" + input.StoreCode);

                    var repository = new ProductRepository(context);
                    var partInquire = new PartInquireDecorator(repository, input);
                    var output = partInquire.Build();
                    buf.Append(output);
                    products.AddRange(partInquire.SearchItems);

                  //  if (input.PartNumber == "340018")
                        //break;
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message + " " + e.StackTrace);
                    var errmsg = "Failed to Lookup a part";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(xml, trackNum, 4102, errmsg));
                    debugln(buf.ToString());
                }
            }
        }

        void appendCartResponseItems(StringBuilder buf, String xml)
        {
            int n = 0;
            String val;

            while ((val = AppSDKXMLUtil.getNthTagValue("RequestItem", xml, n++)) != null)
            {
                String id = AppSDKXMLUtil.getTagValue("ID", val);
                String pnum = AppSDKXMLUtil.getTagValue("PartNum", val);
                String lineCode = AppSDKXMLUtil.getTagValue("MfgCode", val);
                String reqQty = AppSDKXMLUtil.getTagValue("Qty", val);
                String price = AppSDKXMLUtil.getTagValue("Price", val);
                String unitCost = AppSDKXMLUtil.getTagValue("UnitCost", price);
                // ignore all other RequestItem fields

                buf.Append("<ResponseItem>");
                buf.Append("<Status value=\"");
                buf.Append("1");
                buf.Append("\"/><ID>").Append(id).Append("</ID>");
                buf.Append("<PartNum>").Append(pnum).Append("</PartNum>");
                buf.Append("<MfgCode>").Append(lineCode).Append("</MfgCode>");
                buf.Append("<DisplayQty>").Append(reqQty).Append("</DisplayQty>");
                buf.Append("<CoreFlag value=\"No\"/>");

                buf.Append("<Price><UnitCost>").Append(unitCost).Append("</UnitCost></Price>");
                buf.Append("<Msg/>");
                buf.Append("<Name>Normal</Name>");

                buf.Append("</ResponseItem>");
            }
        }

        void appendTireInquiryResponseItems(StringBuilder buf, String xml)
        {
            int n = 0;
            String val;
            String estDelivery = Utils.millisPastEpochToDateTime(Utils.CurrentTimeMillis()).ToString("MM/dd/yyyy");

            try
            {
                while ((val = AppSDKXMLUtil.getNthTagValue("RequestItem", xml, n++)) != null)
                {
                    String id = AppSDKXMLUtil.getTagValue("ID", val);
                    String tireSize = AppSDKXMLUtil.getTagValue("TireSize", val);
                    String tirePosition = AppSDKXMLUtil.getTagValue("TirePosition", val);
                    String pnum = AppSDKXMLUtil.getTagValue("PartNum", val);
                    String lineCode = AppSDKXMLUtil.getTagValue("MfgCode", val);
                    String reqQty = AppSDKXMLUtil.getTagValue("Qty", val);
                    String width = "";
                    String ratio = "";
                    String diameter = "";
                    if (tireSize != null && tireSize.Length > 0)
                    {
                        width = AppSDKXMLUtil.getTagValue("Width", tireSize);
                        ratio = AppSDKXMLUtil.getTagValue("Ratio", tireSize);
                        diameter = AppSDKXMLUtil.getTagValue("Diameter", tireSize);
                    }
                    // ignore all other RequestItem fields

                    buf.Append("<ResponseItem>");
                    buf.Append("<Status value=\"1\"/>");
                    buf.Append("<ID>").Append(id).Append("</ID>");
                    if (pnum != null && pnum.Length > 0)
                    {
                        buf.Append("<PartNum>").Append(pnum).Append("</PartNum>");
                        buf.Append("<MfgCode>").Append(lineCode).Append("</MfgCode>");
                    }
                    else
                    {
                        buf.Append("<PartNum>20960</PartNum>");
                        buf.Append("<MfgCode>MICH</MfgCode>");
                    }
                    buf.Append("<DisplayQty>").Append(reqQty).Append("</DisplayQty>");
                    buf.Append("<QtyBuyInc>1</QtyBuyInc>");
                    buf.Append("<RoundedQtyBuyInc>").Append(reqQty).Append("</RoundedQtyBuyInc>");
                    buf.Append("<Desc>Michelin Defender GRNX</Desc>");

                    buf.Append("<ProductType>Tires</ProductType>");
                    buf.Append("<BrandName>MICHELIN</BrandName>");
                    buf.Append("<ModelName>Defender GRNX</ModelName>");
                    buf.Append("<logoURL>http://tbc.scene7.com/is/image/TBCCorporation/michelin</logoURL>");
                    buf.Append("<SpeedRating>T</SpeedRating>");
                    buf.Append("<LoadRange>SL</LoadRange>");
                    buf.Append("<LoadIndexRange>102</LoadIndexRange>");
                    buf.Append("<TreadPattern>NO</TreadPattern>");
                    buf.Append("<AllTerrain>NO</AllTerrain>");
                    buf.Append("<SideWall>Black</SideWall>");
                    buf.Append("<RunFlat>Y</RunFlat>");
                    buf.Append("<LightTruck>Y</LightTruck>");
                    if (width.Length > 0)
                    {
                        buf.Append("<TireSize>");
                        buf.Append("  <Width>").Append(width).Append("</Width>");
                        buf.Append("  <Ratio>").Append(ratio).Append("</Ratio>");
                        buf.Append("  <Diameter>").Append(diameter).Append("</Diameter>");
                        buf.Append("</TireSize>");
                    }
                    else
                    {
                        buf.Append("<TireSize/>");
                    }
                    if (tirePosition != null && tirePosition.Length > 0)
                    {
                        buf.Append("<TirePosition>").Append(tirePosition).Append("</TirePosition>");
                    }
                    else
                    {
                        buf.Append("<TirePosition/>");
                    }
                    buf.Append("<FreightPerTire>0</FreightPerTire>");
                    buf.Append("<EstimatedAvailability>In Stock</EstimatedAvailability>");
                    buf.Append("<EstimatedDelivery>NextTruck</EstimatedDelivery>");
                    buf.Append("<EstimatedDeliveryDate>").Append(estDelivery).Append("</EstimatedDeliveryDate>");
                    buf.Append("<Price><UnitCost>128.00</UnitCost></Price>");
                    buf.Append("<AltLocAvailFlag value=\"No\"/>");

                    buf.Append("</ResponseItem>");
                }
            }
            catch (Exception e)
            {
                println(true, "Caught Exception " + e.Message, e);
            }
        }

        void appendTireOrderResponseItems(StringBuilder buf, String xml)
        {
            int n = 0;
            String val;

            while ((val = AppSDKXMLUtil.getNthTagValue("RequestItem", xml, n++)) != null)
            {
                String id = AppSDKXMLUtil.getTagValue("ID", val);
                String pnum = AppSDKXMLUtil.getTagValue("PartNum", val);
                String lineCode = AppSDKXMLUtil.getTagValue("MfgCode", val);
                String reqQty = AppSDKXMLUtil.getTagValue("Qty", val);
                // ignore all other RequestItem fields

                buf.Append("<ResponseItem>");
                buf.Append("<Status value=\"");
                buf.Append("1");
                buf.Append("\"/><ID>").Append(id).Append("</ID>");
                buf.Append("<PartNum>").Append(pnum).Append("</PartNum>");
                buf.Append("<MfgCode>").Append(lineCode).Append("</MfgCode>");
                buf.Append("<DisplayQty>").Append(reqQty).Append("</DisplayQty>");
                buf.Append("<QtyBuyInc>1</QtyBuyInc>");
                buf.Append("<RoundedQtyBuyInc>").Append(reqQty).Append("</RoundedQtyBuyInc>");
                buf.Append("<Desc>Michelin Defender GRNX</Desc>");
                buf.Append("<Price><UnitCost>128.00</UnitCost></Price>");
                buf.Append("<Msg/>");

                buf.Append("</ResponseItem>");
            }
        }

        void appendInvoiceResponseItems(StringBuilder buf, String xml)
        {
            int n = 0;
            String val;
            // this example code does not show the logic behind building an InvoiceResponse
            // See the SDK Docs for more details on how the ACXInvoiceRequest should be implemented.
            while ((val = AppSDKXMLUtil.getNthTagValue("RequestItem", xml, n++)) != null)
            {
                String id = AppSDKXMLUtil.getTagValue("ID", val);
                String ponum = AppSDKXMLUtil.getTagValue("PONumber", val);
                String innum = AppSDKXMLUtil.getTagValue("InvoiceNumber", val);
                String total = AppSDKXMLUtil.getTagValue("TotalAmount", val);

                buf.Append("<InvoiceResponse>");
                buf.Append("<ID>").Append(id).Append("</ID>");
                if (ponum != null && ponum.Length > 0)
                {
                    buf.Append("<InvoiceNumber>12345</InvoiceNumber>");
                    buf.Append("<InvoiceDate>11/27/17</InvoiceDate>");
                    buf.Append("<InvoiceStatus value=\"1\"/>");
                    if (ponum.Equals("*"))
                    {
                        buf.Append("<PONumber>PO5553</PONumber>");
                    }
                    else
                    {
                        buf.Append("<PONumber>").Append(ponum).Append("</PONumber>");
                    }
                    if (total != null && total.Length > 0)
                    {
                        buf.Append("<TotalAmount>").Append(total).Append("</TotalAmount>");
                    }
                    else
                    {
                        buf.Append("<TotalAmount>10.00</TotalAmount>");
                    }
                    buf.Append("<BuyerName>Fred Smidwick</BuyerName>");
                }
                if (innum != null && innum.Length > 0)
                {
                    buf.Append("<InvoiceNumber>").Append(innum).Append("</InvoiceNumber>");
                    buf.Append("<InvoiceDate>11/27/17</InvoiceDate>");
                    buf.Append("<InvoiceStatus value=\"5\"/>");
                    buf.Append("<PONumber>PO12345</PONumber>");
                    if (total != null && total.Length > 0)
                    {
                        buf.Append("<TotalAmount>").Append(total).Append("</TotalAmount>");
                    }
                    else
                    {
                        buf.Append("<TotalAmount>10.00</TotalAmount>");
                    }
                    buf.Append("<BuyerName>Fred Smidwick</BuyerName>");
                }
                buf.Append("</InvoiceResponse>");
            }
        }

        String processCatalogProfile(String xml, String trackNum, int timeout)
        {
            // refer to comments in appendMCLResponse() function
            // for details about the Catalog Profile file.

            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_CATALOG_PROFILE_RESPONSE));
            buf.Append("<ACXCatalogProfileResponse>");

            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));
            buf.Append("<CatalogResponse><ACXTrackNum>");
            buf.Append(trackNum);
            buf.Append("</ACXTrackNum>");

            if (appendMCLResponse(buf, xml, trackNum) < 0)
            {
                // buf was turned into a notification response so don't add
                // anything more to it. just return.
                debugln("appendMCLResponse failed");
                return buf.ToString();
            }

            buf.Append("</CatalogResponse>");
            buf.Append("</ACXCatalogProfileResponse>");

            return buf.ToString();
        }

        String processProductCoverage(String xml, String trackNum, int timeout)
        {
            // refer to comments in appendProductCoverageResponse() function
            // for details about the Product Coverage File.

            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_PRODUCT_COVERAGE_RESPONSE));
            buf.Append("<ACXProductCoverageResponse>");

            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            if (appendProductCoverageResponse(buf, xml, trackNum) < 0)
            {
                // buf was turned into a notification response so don't add
                // anything more to it. just return.
                debugln("appendMCLResponse failed");
                return buf.ToString();
            }

            buf.Append("</ACXProductCoverageResponse>");

            return buf.ToString();
        }


        String processProductLineMap(String xml, String trackNum, int timeout)
        {
            // refer to comments in appendProductLineMapResponse() function
            // for details about the Product Line Map File.

            StringBuilder buf = new StringBuilder(1024);
            buf.Append(AppSDKXMLUtil.makeResponseHeader(xml, AppSDKConst.TRANS_TYPE_PRODUCT_LINE_MAP_RESPONSE));
            buf.Append("<ACXProductLineMapResponse>");

            appendEnvelope(buf, AppSDKXMLUtil.getTagValue("BuyPartnerID", xml));

            if (appendProductLineMapResponse(buf, xml, trackNum) < 0)
            {
                // buf was turned into a notification response so don't add
                // anything more to it. just return.
                return buf.ToString();
            }

            buf.Append("</ACXProductLineMapResponse>");
            return buf.ToString();
        }

        int appendMCLResponse(StringBuilder buf, String reqXml, String acxTrackNum)
        {
            String errmsg = null;
            String catalogRequest;
            if ((catalogRequest = AppSDKXMLUtil.getTagValue("CatalogRequest", reqXml)) != null)
            {
                String sellPartnerID, custNum, mclRequest, type;
                sellPartnerID = AppSDKXMLUtil.getTagValue("SellPartnerID", catalogRequest);
                custNum = AppSDKXMLUtil.getTagValue("CustNum", catalogRequest);
                mclRequest = AppSDKXMLUtil.getTagValue("MCLRequest", catalogRequest);
                type = AppSDKXMLUtil.getTagValue("Type", mclRequest);
                if ((type == null) || (type != null && type.Length == 0))
                {
                    // possible values for type are "Dealer" or "Primary"
                    // If the type is empty then set type to the default of "Dealer"
                    type = "Dealer";
                }
                // Use SellPartnerID and type to determine the full path of the MCL file
                // on your system. The custNum should not be used to determine the location of the
                // MCL file. The MCL file should apply to the Seller location as a whole and 
                // not to any one customer account. You could have multiple MCL files if your
                // Seller System supports multiple SellPartnerIDs.
                // Here I will just assume the file is located up one directory from where this program is run
                // and is named "exampleMCLfile".
                String mclFileName = "../exampleMCLfile";

                // The raw MCL file is not what gets transmitted back to AConneX. You must
                // first compress the file using a compression program provided by 
                // the Activant IS team. The name of the compression program is 
                // cpmcl and has been provided in this SDK for the OS's that are supported.

                // The FileSize and TimeStamp returned in the response must be values 
                // derived from the raw MCL file and not of the compressed file.

                long file_length;
                long last_mod_time;

                if (!File.Exists(mclFileName))
                {
                    errmsg = "File " + mclFileName + " not found";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }
                FileInfo file = new FileInfo(mclFileName);

                file_length = file.Length;
                last_mod_time = Utils.millisPastEpochFromDateTime(file.LastWriteTime);

                // The TimeStamp should be the last modification time of the raw MCL file
                // in the format shown below while the FileSize should be the actual 
                // file size of the raw MCL file.
                // yyyy/mm/dd hh:mm:ss
                String file_length_str = file_length.ToString();
                String time_stamp_str = Utils.millisPastEpochToDateTime(last_mod_time).ToString("yyyy'/'MM'/'dd' 'HH':'mm':'ss");

                buf.Append("<MCLResponse>");
                buf.Append("<TimeStamp>");
                buf.Append(time_stamp_str);
                buf.Append("</TimeStamp><FileSize>");
                buf.Append(file_length_str);
                buf.Append("</FileSize><File>");

                // The file should be appended here Base64 encoded.
                // First compress the file
                String cmpmclFileName = "cmpexampleMCLfile";
                File.Delete(cmpmclFileName);


                // Now compress the file
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardOutput = true;
                startInfo.FileName = "../../cpmcl/cpmcl.exe";
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.Arguments = mclFileName + " " + cmpmclFileName;
                try
                {
                    // Start the process with the info we specified.
                    // Call WaitForExit and then the using statement will close.
                    using (Process exeProcess = Process.Start(startInfo))
                    {
                        StreamReader sr = exeProcess.StandardOutput;
                        String line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            debugln(line);
                        }
                        sr.Close();
                        exeProcess.WaitForExit();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + " " + e.StackTrace);
                    errmsg = "Failed to execute cpmcl to compress the file.";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }

                if (!File.Exists(cmpmclFileName))
                {
                    errmsg = "Failed to convert " + mclFileName + " to " + cmpmclFileName;
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }
                FileInfo cmpfile = new FileInfo(cmpmclFileName);

                String file_contents = null;
                // read the file into file_contents
                try
                {
                    file_contents = Utils.readFile(cmpmclFileName);
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.Message + " " + ioe.StackTrace);
                    errmsg = "Failed to read compressed file.";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;

                }
                String encoded_contents = null;
                try
                {
                    encoded_contents = Convert.ToBase64String(new UTF8Encoding(true).GetBytes(file_contents));
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.Message + " " + ioe.StackTrace);
                    errmsg = "Failed to Base64 encode compressed file.";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;

                }

                buf.Append(encoded_contents);

                buf.Append("</File>");
                buf.Append("</MCLResponse>");

            } // CatalogRequest tag exists
            else
            {
                errmsg = "CatalogRequest tag not found";
                buf.Remove(0, buf.Length);
                buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4002, errmsg));
                debugln(buf.ToString());
                return -1;
            }
            return 0;
        }

        private String getNextString(BufferedStream bstr)
        {
            StringBuilder returnBuffer = new StringBuilder();

            while (true)
            {
                int b = bstr.ReadByte();
                if (b == -1)               // meaning we have reached end of file
                    throw new EndOfStreamException();

                if (b == '\0')             // If we have just read a null byte
                {
                    break;
                }
                else
                {
                    returnBuffer.Append((char)b);
                }
            }
            return returnBuffer.ToString();
        }

        Boolean fileUpToDate(long file_length, long last_mod_time, String timestamp, String filesize)
        {
            // Now, check whether we have an updated file or not
            if (((filesize != null) && (filesize.Length > 0)) &&
               ((timestamp != null) && (timestamp.Length > 0)))
            {
                // We need to see whether the time stamp and the filesize have
                // changed or not

                debugln("filesize = " + filesize);
                long file_size = int.Parse(filesize);
                debugln("file_size  = " + file_size);

                DateTime last_mod_date = Utils.millisPastEpochToDateTime(last_mod_time);
                String last_mod_date_str = Utils.millisPastEpochToDateTime(last_mod_time).ToString("yyyy'/'MM'/'dd' 'HH':'mm':'ss");
                DateTime timestamp_date = Convert.ToDateTime(timestamp);
                long timestamp_time = Utils.millisPastEpochFromDateTime(timestamp_date);

                debugln("last_mod_date  = " + last_mod_date_str);
                debugln("timestamp      = " + timestamp);
                debugln("last_mod_time  = " + last_mod_time);
                debugln("timestamp_time = " + timestamp_time);
                debugln("file_length    = " + file_length);
                debugln("file_size      = " + file_size);
                if (!last_mod_date.Equals(timestamp_date) ||
                                (file_size != file_length))
                    return false;
                else
                    return true;
            }
            return false;
        }


        int appendProductCoverageResponse(StringBuilder buf, String reqXml, String acxTrackNum)
        {
            String errmsg = null;
            String productCoverageRequest;
            if ((productCoverageRequest = AppSDKXMLUtil.getTagValue("ProductCoverageRequest", reqXml)) != null)
            {
                String sellPartnerID = null, custNum = null, lastVersion = null, timeStamp = null, fileSize = null;

                sellPartnerID = AppSDKXMLUtil.getTagValue("SellPartnerID", productCoverageRequest);
                custNum = AppSDKXMLUtil.getTagValue("CustNum", productCoverageRequest);
                lastVersion = AppSDKXMLUtil.getTagValue("LastVersion", productCoverageRequest);

                if (lastVersion != null)
                {
                    timeStamp = AppSDKXMLUtil.getTagValue("TimeStamp", lastVersion);
                    fileSize = AppSDKXMLUtil.getTagValue("FileSize", lastVersion);
                }

                // Use SellPartnerID to determine the full path of the product coverage file
                // on your system. The custNum should not be used to determine the location of the
                // product coverage file. The product coverage file should apply to the Seller System
                // as a whole and not to any one customer account.
                // Here I will just assume the file is located up one directory from where this program is run
                // and is named "examplePCLfile".
                String pclFileName = "../examplePCLfile";

                // The examplePCLfile is formatted in a particular way described below where the file gets
                // read in. Your file can be formated any way you want as long as the end result is that
                // you are able to fill in this response document according to the format AConneX expects.

                // The AConneX user requesting the Product Coverage file wants to make sure they have the
                // latest version of the file from the Seller System. They do this by keeping track of the
                // file size and last modified time stamp of the file on the Seller System. This means the
                // Seller System should only build or modify this file whenever there are changes to the 
                // products being covered by the system. By making sure the file does not change unless the
                // products have changed it prevents needless transmission of this file back to the AConneX
                // user when there have not been any changes.

                // The FileSize and TimeStamp are given in the request and represent the FileSize and
                // TimeStamp of the product coverage file as of the last time they requested it. 
                // You should compare this FileSize and TimeStamp given with the actual file size and 
                // last modified time stamp of your current Product Coverage file and if they are different
                // then the current file should be included in the response along with a status of "UpdateAvailable".
                //	
                // If this is the first time the file has been requested the FileSize and TimeStamp will be empty
                // so the file should be included in the response along with the status of "UpdateAvailable".
                //
                // If the FileSize and TimeStamp are the same then the AConneX user has the latest file so the
                // current file should not be included in the response and the status of "UpdateNotAvailable" should
                // be returned.

                buf.Append("<ProductCoverageResponse>");
                buf.Append("<ACXTrackNum>");
                buf.Append(acxTrackNum);
                buf.Append("</ACXTrackNum><CurrentStatus value=\"");

                long file_length;
                long last_mod_time;
                if (!File.Exists(pclFileName))
                {
                    errmsg = "File " + pclFileName + " not found";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }
                FileInfo file = new FileInfo(pclFileName);

                file_length = file.Length;
                last_mod_time = Utils.millisPastEpochFromDateTime(file.LastWriteTime);


                // The TimeStamp should be the last modification time of the raw MCL file
                // in the format shown below while the FileSize should be the actual 
                // file size of the raw MCL file.
                // yyyy/mm/dd hh:mm:ss
                String file_length_str = file_length.ToString();
                String time_stamp_str = Utils.millisPastEpochToDateTime(last_mod_time).ToString("yyyy'/'MM'/'dd' 'HH':'mm':'ss");


                if ((fileSize == null) || (fileSize != null && fileSize.Length == 0) &&
                                (timeStamp == null) || (timeStamp != null && timeStamp.Length == 0))
                {
                    // If the file size and timestamp are not given then we need to 
                    // set the status to "UpdateAvailable" and send the file back.
                    buf.Append("UpdateAvailable\"/>");
                }
                else
                {
                    // compare the fileSize and timeStamp given with the actual file size and timestamp of the
                    // current Product Coverage file.
                    // If they are the same then set the status to "UpdateNotAvailable".
                    // If they are different then set the status to "UpdateAvailable" and include the file.

                    if (fileUpToDate(file_length, last_mod_time, timeStamp, fileSize))
                    {
                        // file is up to date so no need to send the file. just set the status and return
                        buf.Append("UpdateNotAvailable\"/>");
                        buf.Append("</ProductCoverageResponse>");
                        return 0;
                    }

                    // file is not up to date so set status and fall through to add the file
                    buf.Append("UpdateAvailable\"/>");
                } // no file size given


                // add the file to the response
                buf.Append("<LastVersion><TimeStamp>");
                buf.Append(time_stamp_str);
                buf.Append("</TimeStamp><FileSize>");
                buf.Append(file_length_str);
                buf.Append("</FileSize></LastVersion>");

                String mfgName, mfgCode, partName, partCode;
                try
                {
                    BufferedStream InStream = null;
                    FileStream FStream = null;
                    FStream = new FileStream(pclFileName, FileMode.Open, FileAccess.Read);
                    InStream = new BufferedStream(FStream);
                    try
                    {
                        // The following while loop will read the content of the PCL file
                        // and expects the file to be in the following format:
                        // each record conists of 4 null terminated strings.
                        // MfgName     40 chars max + null terminating byte e.g. "AC Delco"
                        // MfgCode     5  chars max + null terminating byte e.g. "ACD"
                        // SubLineName 60 chars max + null terminating byte e.g. "Engine Filters"
                        // SubLineCode 5  chars max + null terminating byte e.g. "14"
                        // The fields do not need to be and should not be padded to their max size.
                        // The null terminating byte will separate each field.

                        //	Now read fields from the file until we are done
                        while (true)
                        {
                            mfgName = getNextString(InStream);
                            mfgCode = getNextString(InStream);
                            partName = getNextString(InStream);
                            partCode = getNextString(InStream);

                            if ((mfgName.Length == 0) &&
                                            (mfgCode.Length == 0) &&
                                            (partName.Length == 0) &&
                                            (partCode.Length == 0))
                                break;
                            buf.Append("<Product><MfgName>");
                            buf.Append(mfgName);
                            buf.Append("</MfgName><MfgCode>");
                            buf.Append(mfgCode);
                            buf.Append("</MfgCode><PartName>");
                            buf.Append(partName);
                            buf.Append("</PartName><PartCode>");
                            buf.Append(partCode);
                            buf.Append("</PartCode></Product>");
                        }
                    }
                    catch (EndOfStreamException)
                    {
                        debugln("reached end of product coverage file");
                    }

                }
                catch (FileNotFoundException)
                {
                    errmsg = "Error opening " + pclFileName + " file, not found.";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }
                catch (IOException)
                {
                    errmsg = "Error reading " + pclFileName + " file.";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }

                buf.Append("</ProductCoverageResponse>");
            } // ProductCoverageRequest tag exists
            else
            {
                errmsg = "ProductCoverageRequest tag not found.";
                buf.Remove(0, buf.Length);
                buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4002, errmsg));
                debugln(buf.ToString());
                return -1;
            }
            return 0;
        }



        int appendProductLineMapResponse(StringBuilder buf, String reqXml, String acxTrackNum)
        {
            // This request only applies to Seller systems that have the concept of mapping their 
            // Line codes to those of another Seller systems Line codes. If your Seller system 
            // does not have this concept then you should respond with an ACXNotificationResponse 
            // with an Error Code of 4200 (File Status Error) and MSG of something like 
            // "Product Line Map not supported.". For Example an Activant JCON system has a Map 
            // of its Line codes to that of the Activant ADIS warehouse's Line codes that it 
            // buys parts from. An eStore application can buy parts from the warehouse on behalf
            // of the Store so the eStore uses this mapping to provide the correct Line codes to
            // the warehouse in relation to the Stores Line codes. 

            String errmsg = null;
            String productLineMapRequest;
            if ((productLineMapRequest = AppSDKXMLUtil.getTagValue("ProductLineMapRequest", reqXml)) != null)
            {
                String sellPartnerID = null, custNum = null, mapToPartnerID = null;
                String networkDetails = null, system = null, nodeID = null, location = null, clientNum = null;

                sellPartnerID = AppSDKXMLUtil.getTagValue("SellPartnerID", productLineMapRequest);
                custNum = AppSDKXMLUtil.getTagValue("CustNum", productLineMapRequest);
                mapToPartnerID = AppSDKXMLUtil.getTagValue("MapToPartnerID", productLineMapRequest);
                networkDetails = AppSDKXMLUtil.getTagValue("NetworkDetails", reqXml);

                // The MapToPartnerID should be enough to identify the Product Line Map
                // File to return. If need be, however, there is a NetworkDetails 
                // element that contains more information that may help in identifying
                // the correct file to return.

                // Find out what type of system the MapToPartnerID belongs to.
                // The NetworkDetails element will contain one element whoes tag name
                // represents the type of system it is and this element contains 
                // information about the system such as its NodeID and Location number
                // which can be used to more specifically identify which locaiton on that
                // system the request is refering to. Currently valid system types 
                // are "Ultimate", "Eclipse", "JCON", "Prism", and "ADIS". If 
                // none of these exist in the NetworkDetails element then the system
                // is a NonActivant system.
                if ((system = AppSDKXMLUtil.getTagValue("Ultimate", networkDetails)) != null && system.Length > 0)
                {
                    nodeID = AppSDKXMLUtil.getTagValue("Node", system);
                    location = AppSDKXMLUtil.getTagValue("Location", system);
                    system = "Ultimate";
                }
                else if ((system = AppSDKXMLUtil.getTagValue("Eclipse", networkDetails)) != null && system.Length > 0)
                {
                    nodeID = AppSDKXMLUtil.getTagValue("Node", system);
                    location = AppSDKXMLUtil.getTagValue("Location", system);
                    system = "Eclipse";
                }
                else if ((system = AppSDKXMLUtil.getTagValue("JCON", networkDetails)) != null && system.Length > 0)
                {
                    nodeID = AppSDKXMLUtil.getTagValue("Node", system);
                    location = AppSDKXMLUtil.getTagValue("Location", system);
                    system = "JCON";
                }
                else if ((system = AppSDKXMLUtil.getTagValue("Prism", networkDetails)) != null && system.Length > 0)
                {
                    nodeID = AppSDKXMLUtil.getTagValue("Node", system);
                    location = AppSDKXMLUtil.getTagValue("Location", system);
                    system = "Prism";
                }
                else if ((system = AppSDKXMLUtil.getTagValue("ADIS", networkDetails)) != null && system.Length > 0)
                {
                    nodeID = AppSDKXMLUtil.getTagValue("Node", system);
                    location = AppSDKXMLUtil.getTagValue("Location", system);
                    clientNum = AppSDKXMLUtil.getTagValue("ClientNum", system);
                    system = "ADIS";
                }
                else
                {
                    system = "NonActivant";
                }

                // Use SellPartnerID, MapToPartnerID, system to determine the full path of
                // the PLM file on your system. The custNum should not be used to determine 
                // the location of the PLM file. The PLM file should apply to the Seller 
                // location as a whole and not to any one customer account. You could have 
                // multiple PLM files if your Seller System needs to map sublines to multiple 
                // other systems.
                // Here I will just assume the file is located up one directory from where this program is run
                // and is named "examplePLMfile".
                String plmFileName = "../examplePLMfile";


                // Your system may already have your own format for a conversion file. For this
                // example the PLM file to be read has the following format:
                // each record conists of 4 null terminated strings.
                // The SellerMfgCode and SellerSubLineCode are your seller systems codes.
                //   SellerMfgCode     5  chars max + null terminating byte e.g. "ACD"
                //   SellerSubLineCode 5  chars max + null terminating byte e.g. "14"
                //   MappedMfgCode     5  chars max + null terminating byte e.g. "AC"
                //   MappedSubLineCode 5  chars max + null terminating byte e.g. "14"
                // The fields do not need to be and should not be padded to their max size.
                // The null terminating byte will separate each field.
                // The file contains only those line codes that are different. If a
                // particular line code is the same on your system as it is on the vendor
                // system then it should not be in this file.


                if (!File.Exists(plmFileName))
                {
                    errmsg = "File " + plmFileName + " not found";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }
                FileInfo file = new FileInfo(plmFileName);


                buf.Append("<ProductLineMapResponse>");
                buf.Append("<ACXTrackNum>");
                buf.Append(acxTrackNum);
                buf.Append("</ACXTrackNum><SellPartnerID>");
                buf.Append(sellPartnerID);
                buf.Append("</SellPartnerID><MapToPartnerID>");
                buf.Append(mapToPartnerID);
                buf.Append("</MapToPartnerID>");

                String sellermfgcode, sellersublinecode, mappedmfgcode, mappedsublinecode;

                try
                {
                    BufferedStream InStream = null;
                    FileStream FStream = null;
                    FStream = new FileStream(plmFileName, FileMode.Open, FileAccess.Read);
                    InStream = new BufferedStream(FStream);
                    try
                    {
                        while (true)
                        {
                            sellermfgcode = getNextString(InStream);
                            sellersublinecode = getNextString(InStream);
                            mappedmfgcode = getNextString(InStream);
                            mappedsublinecode = getNextString(InStream);

                            if ((sellermfgcode.Length == 0) &&
                                    (sellersublinecode.Length == 0) &&
                                    (mappedmfgcode.Length == 0) &&
                                    (mappedsublinecode.Length == 0))
                                break;
                            buf.Append("<ProductLineMapItem><SellerMfgCode>");
                            buf.Append(sellermfgcode);
                            buf.Append("</SellerMfgCode><SellerSubline>");
                            buf.Append(sellersublinecode);
                            buf.Append("</SellerSubline><MappedMfgCode>");
                            buf.Append(mappedmfgcode);
                            buf.Append("</MappedMfgCode><MappedSubline>");
                            buf.Append(mappedsublinecode);
                            buf.Append("</MappedSubline></ProductLineMapItem>");
                        }
                    }
                    catch (EndOfStreamException)
                    {
                        debugln("reached end of product line map file");
                    }

                }
                catch (FileNotFoundException)
                {
                    errmsg = "Error opening " + plmFileName + " file, not found.";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }
                catch (IOException)
                {
                    errmsg = "Error reading " + plmFileName + " file.";
                    buf.Remove(0, buf.Length);
                    buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4201, errmsg));
                    debugln(buf.ToString());
                    return -1;
                }
                buf.Append("</ProductLineMapResponse>");

            } // ProductLineMapRequest tag exists
            else
            {
                errmsg = "ProductLineMapRequest tag not found.";
                buf.Remove(0, buf.Length);
                buf.Append(createNotificationResponse(reqXml, acxTrackNum, 4002, errmsg));
                debugln(buf.ToString());
                return -1;
            }
            return 0;
        }


        public void println(Boolean stdout, String msg, Exception e)
        {
            try
            {
                if (e != null)
                {
                    if (stdout && !SellerProperties.Instance.SILENT)
                    {
                        System.Console.WriteLine(msg);
                        System.Console.WriteLine(e.StackTrace);
                    }
                    logger.Info(msg, e);
                }
                else
                {
                    if (stdout && !SellerProperties.Instance.SILENT)
                    {
                        System.Console.WriteLine(msg);
                    }
                    logger.Info(msg);
                }
            }
            catch (Exception) { }
        }

        /**
        * Print a debug message to standard out.
        */
        public void debugln(Object obj)
        {
            try
            {
                if (SellerProperties.Instance.VERBOSE) System.Console.WriteLine(obj);
            }
            catch (Exception) { }
        }

        /**
        * Print an info message to standard out.
        */
        public void println(Object obj)
        {
            try
            {
                if (!SellerProperties.Instance.SILENT) System.Console.WriteLine(obj);
            }
            catch (Exception) { }
        }

    }
}