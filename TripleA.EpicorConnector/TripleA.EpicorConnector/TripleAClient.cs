﻿/*
 * Copyright (c) 2012 Epicor Software..  All Rights Reserved.
 *
 * EPICOR SOFTWARE.  MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 * EPICOR SOFTWARE SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * $Workfile:   sellerclient1.cs  $
 * $Id$
 */

using com.epicor.acxclsdk.acxclapi;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace TripleA.EpicorConnector
{
    public class TripleAClient
    {
        private static String progname = "TripleA.EpicorConnector";
        private static log4net.ILog logger = log4net.LogManager.GetLogger("com.epicor.acxclsdk.acxclapi");
        public static Boolean startedbyacxclserver = false;
        public int delay = 0;

        public AcxCLServer acxclserver = null;


        public TripleAClient(string[] args)
        {
            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            // If port is 0
            // then it will allow the system to pick the port it will listen on
            // and then set the AcxCLServer.port to the port number it is actually
            // listening on.
            int port = 0;
            string portSettingStr = ConfigurationManager.AppSettings["port"];
            if (!string.IsNullOrEmpty(portSettingStr))
            {
                AppSDKXMLUtil.println(true, logger, "getting port from app.config " + portSettingStr, null);
                port = Int32.Parse(portSettingStr); 
            }
            
            String portstr = AppSDKXMLUtil.memberAfter("-p", args);

            if (portstr != null)
            {
                AppSDKXMLUtil.println(true, logger, "overriding port with " + portstr, null);
                port = int.Parse(portstr);
            }

            SellerProperties.Instance.VERBOSE = AppSDKXMLUtil.member("-verbose", args);
            SellerProperties.Instance.SILENT = AppSDKXMLUtil.member("-silent", args);
            String delaystr = AppSDKXMLUtil.memberAfter("-delay", args);
            if (delaystr != null)
                delay = int.Parse(delaystr);

            this.acxclserver = new AcxCLServer(port, args);

            // see if we were started by acxclserver.
            // if we were then the command line will contain a "-x id" 
            // where id is the index into the list of connections acxclserver has 
            // created for this process.
            //
            // If we were started by acxclserver then our program should exit when
            // we have lost all connections from acxclserver.
            String ConnID = AppSDKXMLUtil.memberAfter("-x", args);
            if (ConnID != null)
            {
                startedbyacxclserver = true;
                progname = progname + "_" + ConnID;
            }

            // If the port is 0 then we must have been started as one process per
            // connection so set the one connection only flag so that we will catch an
            // AppSDKLostConnException when our one and only connection to the 
            // acxclserver web server is lost so we can know to exit.
            // If the port is > 0 then it is a well know port so there will only be
            // one process started with multiple connections to it. We will have
            // to rely on receiving a shutdown message from acxclserver to know when
            // to exit.
            if (port == 0)
                this.acxclserver.setOneConnectionOnly(true);

            // see if a gateway hostname other than www.aconnex.com was specified
            // on the command line (like www.staging.activant.com).
            // If so then set the new host name.
            // This is only necessary if your example code uses the AppSDKXMLUtil
            // helper methods makeRequestHeader(), makeResponseHeader() or
            // createACXNotification()
            String gwhost = AppSDKXMLUtil.memberAfter("-gwhost", args);
            if (gwhost != null)
            {
                AppSDKXMLUtil.println(true, logger, "overriding gwhost with " + gwhost, null);
                AppSDKXMLUtil.setHostname(gwhost);
            }
            string directoryName = new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName;
            AppSDKXMLUtil.println(true, logger, "cwd = " + System.Environment.CurrentDirectory + " executable dir = " +
                directoryName, null);
            // Change our current directory to where we were started from so we can find some example files
            Directory.SetCurrentDirectory(directoryName);
        }

        public static void Main(string[] args)
        {
            TripleAClient seller = new TripleAClient(args);
            try
            {
                seller.run();
            }
            catch (Exception e)
            {
                AppSDKXMLUtil.println(true, logger, "Main caught Exception " + e, e);

                //Writting in the event log
                System.Diagnostics.EventLog appLog = new System.Diagnostics.EventLog();
                appLog.Source = nameof(TripleA.EpicorConnector);
                appLog.WriteEntry("Main caught Exception: " + e.Message);
            }
        }


        public void run()
        {
            AppSDKXMLUtil.println(false, logger, progname + " entered run", null);
            try
            {
                while (true)
                {
                    MsgInfo msginfo = null;

                    AppSDKXMLUtil.println(true, logger, progname + " Waiting forever for a msg", null);

                    try
                    {
                        msginfo = acxclserver.receive(AppSDKConst.BLOCK_FOREVER);
                    }
                    catch (AppSDKLostConnException le)
                    {
                        // we lost our connection to the acxclserver. It may have
                        // gone down so we should exit now.
                        // we would only get this exception if we were started by 
                        // the acxclserver and we called the acxclserver.setOneConnectionOnly(true);
                        AppSDKXMLUtil.println(true, logger, "We lost connection to acxclserver so exiting", le);
                        return;
                    }


                    if (msginfo.msg != null)
                    {
                        AppSDKXMLUtil.println(true, logger, progname + " got msg [" + (msginfo.msg.Length > 20 ? msginfo.ACXTrackNum : msginfo.msg) + "]", null);

                        if (msginfo.msg.StartsWith("Shutdown"))
                        {
                            // A Shutdown msg indicates the acxclserver process is exiting.
                            //
                            // if we were started by the acxclserver web server then
                            // lets exit our process because it told us to.
                            // Otherwise we will just stay up and wait for a new acxclserver
                            // process to connect to us again.
                            if (startedbyacxclserver)
                            {
                                AppSDKXMLUtil.println(true, logger, progname + " We were told to exit", null);
                                return;
                            }
                        }
                        else
                        {
                            String resp = null;
                            RequestProcessor rp = new RequestProcessor(delay, progname);
                            try
                            {
                                resp = rp.processRequest(msginfo);
                            }
                            catch (ServiceProviderException spe)
                            {

                                resp = rp.createNotificationResponse(msginfo.request, msginfo.ACXTrackNum, spe.getCode(), spe.Message);
                            }
                            catch (Exception e)
                            {
                                resp = rp.createNotificationResponse(msginfo.request, msginfo.ACXTrackNum, ErrorConst.SYS_ERR_GENERAL, e.Message);
                            }
                            AppSDKXMLUtil.println(false, logger, progname + " sending back " + resp, null);
                            acxclserver.send(resp, msginfo);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                AppSDKXMLUtil.println(true, logger, "run caught Exception ", e);

                System.Diagnostics.EventLog appLog = new System.Diagnostics.EventLog();
                appLog.Source = nameof(TripleA.EpicorConnector);
                appLog.WriteEntry("run caught Exception: " + e.Message);
            }
            AppSDKXMLUtil.println(false, logger, progname + " exited run", null);
        }
    }
}
