﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using TripleA.DataAccess.Repositories;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.SearchDecorator.SearchDecorator
{
    public class PartInquireDecorator : ISearch
    {
        private SearchInputDto _input { get; set; }
        private ProductRepository _repository { get; set; }
        public List<ProductDto> SearchItems = new List<ProductDto>();
        public PartInquireDecorator(ProductRepository repository, SearchInputDto input)
        {
            _input = input;
            _repository = repository;
        }

        public string Build()
        {
            StringBuilder buf = new StringBuilder();
            var items = _repository.Search(_input);

            if (!items.Any())
            {
                buf.Append("<ResponseItem>");
                buf.Append("<Status value=\"2\"/>");
                buf.Append($"<ID>{_input.Id}</ID>");
                buf.Append($"<PartNum>{_input.PartNumber}</PartNum>");
                buf.Append($"<MfgCode>{_input.LineCode}</MfgCode>");
                buf.Append($"<PackCode/>");
                buf.Append($"<LocNum/>");       
                buf.Append($"<DisplayQty>0</DisplayQty>");
                buf.Append($"<PackNumUnits/>");
                buf.Append($"<RoundedQtyBuyInc>{_input.RequestQty}</RoundedQtyBuyInc>");
                buf.Append($"<Desc/>");
                buf.Append(@"<CoreFlag value=""No""/>");
                buf.Append($"<Price>" +
                    $"<UnitCost>.00</UnitCost>" +
                    $"<UnitList>.00</UnitList>" +
                    $"</Price>");
                buf.Append($"<Msg/>");
                buf.Append("</ResponseItem>");
            }

            foreach (var item in items)
            {
                item.RequestQty = int.Parse(_input.RequestQty);
                //status 8 = wildcardsearch
                //status 1 = search by partnumber and linecode
                //status 6 = alternative parts only happening during status 6
                buf.Append("<ResponseItem>");
                var statusId = _input.LineCode == "*" ?
                    8 : 1;
                buf.Append($"<Status value=\"{statusId}\" />");
                buf.Append($"<ID>{_input.Id}</ID>");
                buf.Append($"<PartNum>{item.Code}</PartNum>");
                buf.Append($"<MfgCode>{item.LineCode}</MfgCode>");
                buf.Append("<PackCode/>");
                buf.Append("<LocNum/>");
                buf.Append($"<DisplayQty>{item.Qty}</DisplayQty>");
                buf.Append("<PackNumUnits>1</PackNumUnits>");
                buf.Append($"<QtyBuyInc>{_input.RequestQty}</QtyBuyInc>");
                buf.Append($"<RoundedQtyBuyInc>{_input.RequestQty}</RoundedQtyBuyInc>");
                buf.Append($"<Desc>{item.Description}</Desc>");
                buf.Append(@"<CoreFlag value=""Yes""/>");
                buf.Append($"<Price>" +
                    $"<UnitCost>{item.Cost}</UnitCost>" +
                    $"<UnitList>{item.Price}</UnitList>" +
                    $"</Price>");

                var alternativeLocationsDecorator = new AlternativeLocationsDecorator(_repository,
                    new AlternativeLocationsInputDto
                    {
                        PartNumber = _input.PartNumber,
                        StoreCode = _input.StoreCode,
                        LineCode = _input.LineCode
                    });
                var output = alternativeLocationsDecorator.Build();
                buf.Append(output);
                buf.Append("<Msg/>");
                buf.Append("</ResponseItem>");
            }
          
            //in case we are looking an alternative part
            SearchItems.AddRange(items);

            if (_input.LineCode == "*")
            {
                var alternativePartDecorator = new AlternativePartsDecorator(_repository,
                    new AlternativePartInputDto
                    {
                        Id = _input.Id,
                        PartNumber = _input.PartNumber,
                        StoreCode = _input.StoreCode,
                        RequestQty = int.Parse(_input.RequestQty),
                        GroupCode = null,
                        LineCode = _input.LineCode
                    });
                var output = alternativePartDecorator.Build();
                buf.Append(output);
            }

            return buf.ToString();
        }
    }
}
