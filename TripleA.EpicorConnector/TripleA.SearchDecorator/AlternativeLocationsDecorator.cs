﻿using System.Linq;
using System.Text;
using TripleA.DataAccess.Repositories;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.SearchDecorator.SearchDecorator
{
    public class AlternativeLocationsDecorator : ISearch
    {
        private AlternativeLocationsInputDto _input { get; set; }
        private ProductRepository _repository { get; set; }
        public AlternativeLocationsDecorator(ProductRepository repository, AlternativeLocationsInputDto input)
        {
            _input = input;
            _repository = repository;
        }

        public string Build()
        {
            StringBuilder buf = new StringBuilder();
            var alternativeLocations = _repository.GetAlternativeLocations(_input);

            if (!alternativeLocations.Any())
                return string.Empty;

            string alternativeTag = alternativeLocations.Any() ?
                @"<AltLocAvailFlag value=""Yes""/>" : @"<AltLocAvailFlag value=""No""/>";

            buf.Append(alternativeTag);

            foreach (var location in alternativeLocations)
            {
                buf.Append("<AltLoc>");
                buf.Append($"<Name>{location.StoreName}</Name>");
                buf.Append("<Num/>");
                buf.Append($"<PartnerID>6036530010613877</PartnerID>");
                buf.Append($"<DisplayQty>{location.Qty}</DisplayQty>");
                buf.Append("</AltLoc>");
            }

            return buf.ToString();
        }
    }
}
