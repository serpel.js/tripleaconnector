﻿using System.Linq;
using System.Text;
using TripleA.DataAccess.Repositories;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.SearchDecorator.SearchDecorator
{
    public class AlternativePartsDecorator : ISearch
    {
        private AlternativePartInputDto _input { get; set; }
        private ProductRepository _repository { get; set; }
        public AlternativePartsDecorator(ProductRepository repository, AlternativePartInputDto input)
        {
            _input = input;
            _repository = repository;
        }

        public string Build()
        {
            StringBuilder buf = new StringBuilder();
            var alternativeCodes = _repository.GetAlternativeCodes(_input);

            if (!alternativeCodes.Any())
                return string.Empty;
       
            foreach (var item in alternativeCodes)
            {
                buf.Append("<ResponseItem>");
                buf.Append(@"<Status value=""6""/>");
                buf.Append($"<ID>{_input.Id}</ID>");
                buf.Append($"<PartNum>{item.Code}</PartNum>");
                buf.Append($"<MfgCode>{item.LineCode}</MfgCode>");
                buf.Append("<PackCode/>");
                buf.Append($"<DisplayQty>{item.Qty}</DisplayQty>");
                buf.Append("<PackNumUnits>1</PackNumUnits>");
                buf.Append($"<QtyBuyInc>{_input.RequestQty}</QtyBuyInc>");

                buf.Append($"<RoundedQtyBuyInc>{_input.RequestQty}</RoundedQtyBuyInc>");
                buf.Append($"<Desc>{item.Description}</Desc>");
                buf.Append(@"<CoreFlag value=""No""/>");
                buf.Append($"<Price>" +
                    $"<UnitCost>{item.Cost}</UnitCost>" +
                    $"<UnitList>{item.Price}</UnitList>" +
                    $"</Price>");

                var alternativeLocationsDecorator = new AlternativeLocationsDecorator(_repository,
                    new AlternativeLocationsInputDto
                    {
                        PartNumber = _input.PartNumber,
                        StoreCode = _input.StoreCode
                    });
                var alternativeLocationStr = alternativeLocationsDecorator.Build();
                buf.Append(alternativeLocationStr);

                buf.Append("<Msg/>");
                buf.Append("</ResponseItem>");
            }

            return buf.ToString();
        }
    }
}
