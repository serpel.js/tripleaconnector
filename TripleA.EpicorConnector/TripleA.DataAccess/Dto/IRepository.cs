﻿namespace TripleA.DataAccess.Repositories.Dto
{
    interface IRepository<T>
    {
        T Find(int id);
        T FindByCode(string code);
        void Create(T item);
    }
}
