﻿namespace TripleA.DataAccess.Repositories.Dto
{
    public class SearchInputDto
    {
        public string Id { get; set; }
        public string PartNumber { get; set; }
        public string RequestQty { get; set; }
        public string LineCode { get; set; }
        public string BuyerDesc { get; set; }
        public string StoreCode { get; set; }
    }
}
