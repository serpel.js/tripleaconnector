﻿namespace TripleA.DataAccess.Repositories.Dto
{
    public class AlternativePartInputDto
    {
        public string Id { get; set; }
        public string PartNumber { get; set; }
        public int? GroupCode { get; set; }
        public string LineCode { get; set; }
        public string StoreCode { get; set; }
        public int RequestQty { get; set; }
    }
}
