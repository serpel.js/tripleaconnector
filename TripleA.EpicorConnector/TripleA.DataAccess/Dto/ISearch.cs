﻿namespace TripleA.DataAccess.Repositories.Dto
{
    public interface ISearch
    {
        string Build();
    }
}
