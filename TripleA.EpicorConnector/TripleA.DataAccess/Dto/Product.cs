﻿namespace TripleA.DataAccess.Repositories.Dto
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LineCode { get; set; }
        public int GroupCode { get; set; }
        public string StoreName { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public int Qty { get; set; }
        public int RequestQty { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }

        public decimal? SubTotal()
        {
            return Total();
        }

        public decimal? Total()
        {
            return RequestQty * Price;
        }

        public decimal? ISV(decimal isv = 0.15m)
        {
            return Total() - (Total() / (isv + 1));
        } 

        public ProductDto() { }

        public ProductDto(string code, string description, int qty, decimal? price)
        {
            this.Code = code;
            this.Description = description;
            this.Qty = qty;
            this.Price = price;
        }
    }
}
