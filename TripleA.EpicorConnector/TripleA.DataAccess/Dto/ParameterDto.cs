﻿namespace TripleA.DataAccess.Repositories.Dto
{
    public class ParameterDto
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public int? Value { get; set; }
    }
}
