﻿using System;
using System.Collections.Generic;

namespace TripleA.DataAccess.Repositories.Dto
{
    public class OrderDto
    {
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public string OrderIdMp4 { get; set; }
        public string CustomerName { get; set; }
        public string OrderType { get; set; } = "Contado"; //Credito / Contado
        public DateTime OrderDate { get; set; }
        public DateTime OrderEndDate { get; set; }
        public string OrderStatus { get; set; } = "Impresa";
        public string Correlative { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Total { get; set; }
        public decimal ISV { get; set; }
        public decimal Discount { get; set; }
        public int EndDay { get; set; } = 7;
        public int IdCatTiendas { get; set; }
        public int IdCatVendedor { get; set; }
        public string Comments { get; set; }
        public List<OrderDetail> Lines { get; set; }
    }

    public class OrderDetail
    {
        public int Id { get; set; }
        public int Qty { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Price { get; set; }
        public decimal? Subtotal { get; set; }
        public decimal? ISV { get; set; }
        public decimal? Total { get; set; }
    }
}
