﻿namespace TripleA.DataAccess.Repositories.Dto
{
    public class AlternativeLocationsInputDto
    {
        public string PartNumber { get; set; }
        public string StoreCode { get; set; }
        public string LineCode { get; set; }
    }
}
