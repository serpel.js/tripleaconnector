﻿using System.Linq;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.DataAccess.Repositories
{
    public class ParameterRepository: IRepository<cat_parametros>
    {
        private readonly retailsystem_n3a_t1_testEntities _context;

        public ParameterRepository(retailsystem_n3a_t1_testEntities context)
        {
            _context = context;
        }

        public void Create(cat_parametros item)
        {
            throw new System.NotImplementedException();
        }

        public cat_parametros Find(int id)
        {
            return _context.cat_parametros
                .FirstOrDefault(a => a.id_cat_parametros == id);
        }

        public cat_parametros FindByCode(string code)
        {
            return _context.cat_parametros
                .FirstOrDefault(a => a.llave == code);
        }

        public void IncrementValue(cat_parametros value)
        {
            value.valor += 1;
        }
    }
}
