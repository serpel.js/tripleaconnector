﻿using System.Linq;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.DataAccess.Repositories
{
    public class OrderRepository: IRepository<OrderDto>
    {
        private readonly retailsystem_n3a_t1_testEntities _context;

        public OrderRepository(retailsystem_n3a_t1_testEntities context)
        {
            _context = context;
        }

        public void Create(OrderDto item)
        {
            var parameterRepository = new ParameterRepository(_context);
            var correlative = parameterRepository.FindByCode("cotizacion");

            var orderHeader = new cotizacion()
            {
                mp4p_order = item.OrderIdMp4,
                correlativo = correlative.valor,
                id_cat_usuarios = item.UserId,
                id_cat_vendedor = item.UserId,
                id_cat_clientes = item.CustomerId,
                id_cat_tiendas = item.IdCatTiendas,
                nombre_cliente = item.CustomerName,
                comision_venta_lps = 0,
                isPagoAd = false,
                id_comisionista = 0,
                abonado = 0,
                comentarios = "Epicor: "+item.Comments,
                estatus = item.OrderStatus,
                fecha = item.OrderDate,
                fecha_vencimiento = item.OrderEndDate,
                tipo = item.OrderType,
                hora = item.OrderDate,
                subtotal = item.Subtotal,
                total = item.Total,
                isv = item.ISV,
                descuento = item.Discount,
                dias_vencimiento = item.EndDay,
                descuento_pronto_pago = 0,
                ispack = false,
                isforanea = false,
                devolucion = 0,
                isdescuento_aparte = false
            };

            _context.cotizacion.Add(orderHeader);
            _context.SaveChanges();


            foreach (var line in item.Lines)
            {
                var orderDetail = new cotizacion_det()
                {
                    id_cotizacion = orderHeader.id_cotizacion,
                    id_inventario = line.Id,
                    cantidad = line.Qty,
                    precio = line.Price.GetValueOrDefault(0),
                    total = line.Total.GetValueOrDefault(0),
                    isv = line.ISV.GetValueOrDefault(0),
                    subtotal = line.Subtotal.GetValueOrDefault(0),
                    costo = line.Cost.GetValueOrDefault(0),
                    descuento = 0,
                    cant_handle = 0,
                    descuento_pronto_pago = 0,
                    des_cliente = 0,
                    pronto_pago = 0,
                    ispack = false,
                    isdevuelto = false,
                    des_aparte = 0,
                    max_descuento = 0,
                    tot_desc_aparte = 0,
                    id_cotizacion_det = 0,
                    cant_devuelto = 0,
                    isf8precio = false
                };

                _context.cotizacion_det.Add(orderDetail);
            }
     
            //increment and store new correlative value
            parameterRepository.IncrementValue(correlative);
            _context.SaveChanges();
        }

        public OrderDto Find(int id)
        {
            throw new System.NotImplementedException();
        }

        public OrderDto FindByCode(string code)
        {
            throw new System.NotImplementedException();
        }
    }
}
