﻿using System;
using System.Linq;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.DataAccess.Repositories
{
    public class ClientRepository: IRepository<cat_clientes>
    {
        private readonly retailsystem_n3a_t1_testEntities _context;

        public ClientRepository(retailsystem_n3a_t1_testEntities context)
        {
            _context = context;
        }

        public void Create(cat_clientes item)
        {
            throw new NotImplementedException();
        }

        public cat_clientes Find(int id)
        {
            return _context
               .cat_clientes
               .FirstOrDefault(a => a.id_cat_clientes == id
                                 && a.is_mp4p == true);
        }

        public cat_clientes FindByCode(string code)
        {
            return _context
              .cat_clientes
              .FirstOrDefault(a => a.codigo == code
                                && a.is_mp4p == true);
        }
    }
}
