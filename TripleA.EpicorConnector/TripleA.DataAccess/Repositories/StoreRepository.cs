﻿using System;
using System.Linq;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.DataAccess.Repositories
{
    public class StoreRepository : IRepository<cat_tiendas>
    {
        private readonly retailsystem_n3a_t1_testEntities _context;

        public StoreRepository(retailsystem_n3a_t1_testEntities context)
        {
            _context = context;
        }

        public void Create(cat_tiendas item)
        {
            throw new NotImplementedException();
        }

        public cat_tiendas Find(int id)
        {
            return _context
               .cat_tiendas
               .FirstOrDefault(a => a.id_cat_tiendas == id);
        }

        public cat_tiendas FindByCode(string code)
        {
            return _context
                .cat_tiendas
                .FirstOrDefault(a => a.codigo == code);
        }
    }
}
