﻿using System.Linq;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.DataAccess.Repositories
{
    public class SettingsRepository
    {
        private readonly retailsystem_n3a_t1_testEntities _context;

        public SettingsRepository(retailsystem_n3a_t1_testEntities context)
        {
            _context = context;
        }

        public SettingDto GetSettings()
        {
            var settings = _context.configuracion_sistema.FirstOrDefault();

            return new SettingDto() {
                isv = settings.isv
            };
        }
    }
}
