﻿using System.Collections.Generic;
using System.Linq;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.DataAccess.Repositories
{
    public class ProductRepository
    {
        private readonly retailsystem_n3a_t1_testEntities _context;
       
        public ProductRepository(retailsystem_n3a_t1_testEntities context)
        {
            _context = context;
        }

        public List<ProductDto> Search(SearchInputDto input)
        {
            if (input == null)
                return new List<ProductDto>();

            var products = from c in _context.mp4p_consolidado
                           join t in _context.cat_tiendas on c.id_cat_tiendas equals t.id_cat_tiendas
                           where c.referencia_principal == input.PartNumber
                              && t.codigo == input.StoreCode
                           select new ProductDto
                           {
                               Code = input.PartNumber,
                               Description = c.descripcion + " " + c.marca,
                               Brand = c.marca,
                               Qty = (int)c.inventario,
                               Price = c.precio,
                               Cost = c.costo,
                               Id = c.id_inventario,
                               LineCode = c.codigo_linea,
                               GroupCode = c.codigo_grupo,
                               StoreName = t.mp4p
                           };

            if (input.LineCode != "*")
                products = products.Where(a => a.LineCode == input.LineCode);

            return products.ToList();
        }
  
        public List<ProductDto> GetAlternativeCodes(AlternativePartInputDto input)
        {
            if (input.GroupCode != null)
            {
                var query = from c in _context.mp4p_consolidado
                            join ig in _context.cat_Inventario_por_grupo on c.codigo_grupo equals ig.Id_grupo_inventario
                            join t in _context.cat_tiendas on c.id_cat_tiendas equals t.id_cat_tiendas
                            where ig.Id_grupo_inventario == input.GroupCode.GetValueOrDefault()
                              && t.codigo == input.StoreCode
                              //&& c.inventario > 0
                            select new ProductDto
                            {
                                Code = c.referencia_principal,
                                Description = c.descripcion + " " + c.marca,
                                Brand = c.marca,
                                Qty = (int)c.inventario,
                                Price = c.precio,
                                Cost = c.costo,
                                LineCode = c.codigo_linea,
                                GroupCode = c.codigo_grupo,
                                StoreName = t.mp4p,
                                Id = c.id_inventario
                            };

                return query.ToList();
            }

            //FIX to filter alternative codes
            var alternativeParts = from c in _context.mp4p_consolidado
                        join t in _context.cat_tiendas on c.id_cat_tiendas equals t.id_cat_tiendas
                        where c.referencia_principal == input.PartNumber
                          && c.codigo_linea != input.LineCode
                          && c.codigo_linea != "TMP"
                          && t.codigo == input.StoreCode
                          && c.inventario > 0
                        select new ProductDto
                        {
                            Code = c.referencia_principal,
                            Description = c.descripcion + " " + c.marca,
                            Brand = c.marca,
                            Qty = (int)c.inventario,
                            Price = c.precio,
                            Cost = c.costo,
                            LineCode = c.codigo_linea,
                            GroupCode = c.codigo_grupo,
                            StoreName = t.mp4p,
                            Id = c.id_inventario
                        };

            return alternativeParts.ToList();
        }


        public List<ProductDto> GetAlternativeLocations(AlternativeLocationsInputDto input)
        {
            if (string.IsNullOrEmpty(input.PartNumber) && string.IsNullOrEmpty(input.StoreCode))
                return null;

            var products = from m in _context.mp4p_consolidado
                           join t in _context.cat_tiendas on m.id_cat_tiendas equals t.id_cat_tiendas
                           where m.referencia_principal == input.PartNumber
                             && m.codigo_linea == input.LineCode
                             && t.codigo != input.StoreCode
                             && t.mp4p != null
                           select new ProductDto
                           {
                               Code = m.codigo,
                               Description = m.descripcion,
                               Brand = m.marca,
                               Qty = (int)m.inventario,
                               Price = m.precio,
                               Cost = m.costo,
                               LineCode = m.codigo_linea,
                               GroupCode = m.codigo_grupo,
                               StoreName = t.mp4p,
                               Id = m.id_inventario
                           };

            return products.ToList();
        }
    }
}
