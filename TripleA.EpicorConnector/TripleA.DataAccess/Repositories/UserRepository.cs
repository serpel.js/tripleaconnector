﻿using System;
using System.Linq;
using TripleA.DataAccess.Repositories.Dto;

namespace TripleA.DataAccess.Repositories
{
    public class UserRepository: IRepository<cat_usuarios>
    {
        private readonly retailsystem_n3a_t1_testEntities _context;

        public UserRepository(retailsystem_n3a_t1_testEntities context)
        {
            _context = context;
        }

        public void Create(cat_usuarios item)
        {
            throw new NotImplementedException();
        }

        public cat_usuarios Find(int id)
        {
            return _context
               .cat_usuarios
               .FirstOrDefault(a => a.id_cat_usuarios == id);
        }

        public cat_usuarios FindByCode(string code)
        {
            return _context
              .cat_usuarios
              .FirstOrDefault(a => a.usuario == code);
        }
    }
}
