﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System.Configuration;
using System.Threading.Tasks;

namespace TripleA.DataAccess.Repositories
{
    public static class MailService
    {
        public static async Task<Response> SendMessageAsync(MailServiceDto data)
        {
            var apiKey = ConfigurationManager.AppSettings["SENDGRID_API_KEY"];
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(data.From, "MP4 Admin"),
                Subject = data.Subject,
                PlainTextContent = data.PlainTextContent
            };
            msg.AddTo(new EmailAddress(data.AddTo, ""));
            var response = await client.SendEmailAsync(msg);
            return response;
        }
    }

    public class MailServiceDto
    {
        public string AddTo { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string PlainTextContent { get; set; }
    }
}
