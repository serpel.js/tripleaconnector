//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TripleA.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class cotizacion_det
    {
        public int id_cotizacion_det { get; set; }
        public decimal cantidad { get; set; }
        public decimal costo { get; set; }
        public decimal precio { get; set; }
        public decimal des_cliente { get; set; }
        public decimal des_aparte { get; set; }
        public decimal total { get; set; }
        public int id_cotizacion { get; set; }
        public int id_inventario { get; set; }
        public bool isdevuelto { get; set; }
        public decimal pronto_pago { get; set; }
        public decimal descuento { get; set; }
        public decimal subtotal { get; set; }
        public decimal max_descuento { get; set; }
        public decimal descuento_pronto_pago { get; set; }
        public decimal isv { get; set; }
        public decimal tot_desc_aparte { get; set; }
        public Nullable<decimal> cant_devuelto { get; set; }
        public Nullable<decimal> cant_handle { get; set; }
        public Nullable<bool> ispack { get; set; }
        public Nullable<bool> isf8precio { get; set; }
    }
}
