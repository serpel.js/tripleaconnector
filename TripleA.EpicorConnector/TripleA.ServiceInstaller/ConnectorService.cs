﻿using com.epicor.acxclsdk.acxclapi;
using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using TripleA.EpicorConnector;

namespace TripleA.ServiceInstaller
{
    public partial class ConnectorService : ServiceBase
    {
        protected CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        protected TripleAClient seller;
        public ConnectorService()
        {
            InitializeComponent();

            eventLog1 = new EventLog();
            if (!EventLog.SourceExists(base.ServiceName))
            {
                EventLog.CreateEventSource(
                   base.ServiceName, "Logging TripleAConnector");
            }
            eventLog1.Source = base.ServiceName;
            eventLog1.Log = "Logging TripleAConnector";
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("OnStart. " + base.ServiceName);
            Task.Factory.StartNew(() => StartConnectorClient(args), cancelTokenSource.Token);
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("OnStop. " + base.ServiceName);
            cancelTokenSource.Cancel();
            cancelTokenSource.Dispose();
            seller = null;
        }

        private void StartConnectorClient(string[] args)
        {
            seller = new TripleAClient(args);
            try
            {
                seller.run();
            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("TripleAClient caught Exception " + e.Message);
            }
        }
    }
}
