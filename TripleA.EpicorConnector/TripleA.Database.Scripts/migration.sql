use retailsystem_n3a_test
go

-- crear el campo is_mp4p usado para saber si un cliente es parte de mp4
alter table dbo.cat_clientes add [is_mp4p] bit

go
-- se usa para calcular dinamicamete el impuesto de un repuesto.
alter table dbo.configuracion_sistema add isv numeric(18,2)

go
update dbo.configuracion_sistema
   set isv = 0.15

-- se usa para mapear las tienas que existen en mp4
alter table dbo.cat_tiendas add [mp4p] varchar(100)

go
update dbo.cat_tiendas 
   set mp4p = 'Principal'
 where codigo = '01'

 update dbo.cat_tiendas 
   set mp4p = 'Tercera'
 where codigo = '02'

 update dbo.cat_tiendas 
   set mp4p = 'La Ceiba'
 where codigo = '03'

 update dbo.cat_tiendas 
   set mp4p = 'Santa Rosa'
 where codigo = '04'

GO

-- tabla usada para sacar el inventario consolidado
CREATE TABLE [dbo].[consolidado_tiendas](
	[id_consolidado_tiendas] [int] IDENTITY(1,1) NOT NULL,
	[id_inventario] [int] NULL,
	[cantidad] [numeric](18, 0) NULL,
	[id_cat_tiendas] [int] NULL,
 CONSTRAINT [PK_consolidado_tiendas] PRIMARY KEY CLUSTERED 
(
	[id_consolidado_tiendas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--CEDI
insert into consolidado_tiendas(id_inventario,cantidad,id_cat_tiendas) 
select id_inventario,cantidad_ver_facturacion, 1 from vw_cantidad_ver_facturacion

--principal
insert into consolidado_tiendas(id_inventario,cantidad,id_cat_tiendas) 
select id_inventario,cantidad_ver_facturacion, 7 from [retailsystem_n3a_t1_test].dbo.vw_cantidad_ver_facturacion

--Tercera
insert into consolidado_tiendas(id_inventario,cantidad,id_cat_tiendas) 
select id_inventario,cantidad_ver_facturacion, 2 from [retailsystem_n3a_t2_test].dbo.vw_cantidad_ver_facturacion

--Ceiba
insert into consolidado_tiendas(id_inventario,cantidad,id_cat_tiendas) 
select id_inventario,cantidad_ver_facturacion, 3 from [retailsystem_n3a_t3_test].dbo.vw_cantidad_ver_facturacion

--Santa rosa
insert into consolidado_tiendas(id_inventario,cantidad,id_cat_tiendas) 
select id_inventario,cantidad_ver_facturacion, 4 from [retailsystem_n3a_t4_test].dbo.vw_cantidad_ver_facturacion


